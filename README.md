# check-services-manager

`check-services` is a web services testing tool manager.

This is the application for service check planning and the monitoring control panel. It works with the data obtained by the service check probes (https://github.com/sramos/check-services).

Now it only authorize probes and receive up/down service messages from check-services probes


## Instalation

* Install 2.5.1 Ruby version
* Install gems from working directory:

```
  $ bundle install
```

* Edit private credentials

```
  $ EDITOR="vim" rails credentials:edit
```

and configure `secret_key_base` and `telegram` credentials.

```
secret_key_base: generated_secret_key

telegram:
  bot:
    token: bot_token 
    username: bot_name 
```



## To-do

* Full user and probes management
* Dashboard
* Services failure notifies
* Define probe schedules
* Enable probes provisioning endpoint
