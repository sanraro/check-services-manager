# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)


if User.all.count == 0
  admin = User.create name: "admin", rol: :admin, email: "admin@domain.com", password: "password"
  probe = User.create name: "probe", rol: :probe, password: "password"
end
