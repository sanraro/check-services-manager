class User < ApplicationRecord
  has_secure_password

  validates_presence_of :name, :password_digest
  validates_presence_of :email, unless: Proc.new {|usr| usr.rol == 'probe'}
  validates :name, uniqueness: { scope: :rol } 
  validates :email, uniqueness: true, allow_nil: true

  enum rol: [:probe, :admin]
  after_initialize :set_default_rol, :if => :new_record?
  before_save :normalize_email

  def set_default_rol
    self.rol ||= :probe
  end

  def normalize_email
    self.email = nil if self.probe?
  end

  def admin?
    rol == 'admin'
  end

  def probe?
    rol == 'probe'
  end

end
