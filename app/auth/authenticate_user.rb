class AuthenticateUser
  prepend SimpleCommand
  attr_accessor :name, :email, :password

  #this is where parameters are taken when the command is called
  def initialize(name, email, password)
    @name = name
    @email = email
    @password = password
  end
  
  #this is where the result gets returned
  def call
    JsonWebToken.encode(user_id: user.id) if user
  end

  private

  def user
    user = User.find_by_rol_and_name(:probe, name) || User.find_by_rol_and_email(:admin, email)
    return user if user && user.authenticate(password)

    errors.add :user_authentication, 'Invalid credentials'
    nil
  end
end
